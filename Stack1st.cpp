﻿#include <iostream>
#include<string>
#include <stack>

class Stack
{
private:

	std::stack<int> st;
	int a; // Добавление X элементов в стек
	int x; // Задаем размер стека
	int y; // Удаление Y элементов из стека
	int size; // Получившийся размер стека


public:

	Stack()
	{

		std::cout << "Текущий размер стека: " << st.size() << '\n';
		std::cout << '\n' << "Введите новый размер стека: ";
		std::cin >> x;

		std::cout << '\n' << "Добавьте " << x << " элементов int в стек: " << '\n';

		for (int i = 0; i < x; i++)
		{
			std::cin >> a;
			st.push(a);
		}


		std::cout << '\n' << "Верхний элемент стека: " << st.top() << '\n';
		std::cout << "Новый размер стека: " << st.size() << '\n';
		std::cout << "Давайте удалим 0-" << st.size() << " элементов стека: ";
		std::cin >> y;




		if (y >= st.size())
		{
			std::cout << '\n' << "Стек пуст" << '\n';
		}
		else
		{
			for (int i = 0; i < y; i++)
			{
				st.pop();
			}

			std::cout << '\n' << "Верхний элемент стека: " << st.top() << '\n';
			std::cout << "Новый размер стека: " << st.size() << '\n';



			std::cout << '\n' << "Оставшиеся элементы стека: " << '\n';
			while (!st.empty())
			{
				std::cout << st.top() << '\n';
				st.pop();
			}
		}

	}

};

int main()
{
	setlocale(LC_ALL, "rus");

	Stack wasd;

	return 0;

}